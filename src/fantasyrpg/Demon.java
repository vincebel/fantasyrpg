/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Demon extends Creature {
    
    public Demon(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        super(newName, newStrength, newHit);
    }
    
    public String getSpecies(){
        return "Demon"; //FIX THIS
    }
    
    @Override
    public int getDamage(){
        int damage = super.getDamage();
        double demonChance = Math.random();
            if(demonChance < 0.05){
                damage = damage + 50;
                System.out.println("**DEMONIC ATTACK ACTIVATED**");
            }
        return damage;
    }
    
}
