/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Elf extends Creature {
    
    public Elf(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        super(newName, newStrength, newHit);
    }
    
    public String getSpecies(){
        return "Elf";
    }
    
    @Override
    public int getDamage(){
        int damage = super.getDamage();
        double magicChance = Math.random();
        if(magicChance < 0.1){
            damage = damage * 2;
            System.out.println("**DOUBLE DAMAGE**");
        }
        return damage;
    }
    
}
