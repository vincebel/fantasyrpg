/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

import java.util.ArrayList;
import java.util.Scanner;

public class FantasyRPG {
    
    public static void intro(){
        System.out.println("Welcome to FantasyRPG");
        System.out.println("\t1. Add the game players");
        System.out.println("\t2. Players play in turn until only one is left");
        System.out.println("Good luck!\n");
    }
    
    public static ArrayList<Creature> addPlayers(){
        Scanner cin = new Scanner(System.in);
        String name = "";
        String species;
        /* If true, asks for name.
         * False after: entering a name, quitting with > 1 player, unrecognized species (to enter species again)
         */
        boolean namePrompt = true;
        ArrayList<Creature> players = new ArrayList<>();
        
        System.out.println("First, let's add some players:");
        while(!"quit".equals(name)){
            while(namePrompt){
                System.out.print("\nEnter player's name ('quit' when no more players): ");
                name = cin.nextLine();
                if(name.equals("quit")){
                    if(players.size() == 1)
                        System.out.print("How can you play with only 1 player?\n");  
                    else if(players.size() == 0)
                        System.out.print("How can you play with no players?\n");
                    else
                        namePrompt = false;
                }
                else
                    namePrompt = false;
            }
            if(!"quit".equals(name)){
                System.out.println(name + ", what species are you?");
                System.out.println("\tb/B: Balrog");
                System.out.println("\tc/C: Cyberdemon");
                System.out.println("\te/E: Elf");
                System.out.println("\th/H: Human");
                System.out.print("Select " + name + "'s species: ");
                species = cin.nextLine();
                
                int strength;
                int hitPoints;

                if((species.substring(0, 1).equals("b")) || (species.substring(0, 1).equals("B"))){
                    strength = (int)(Math.random() * (20) + 35);
                    hitPoints = (int)(Math.random() * (5) + 10);
                    Balrog creature = new Balrog(name, strength, hitPoints);
                    players.add(creature);
                    namePrompt = true;
                }
                else if((species.substring(0, 1).equals("c")) || (species.substring(0, 1).equals("C"))){
                    strength = (int)(Math.random() * (15) + 45);
                    hitPoints = (int)(Math.random() * (5) + 15);
                    Cyberdemon creature = new Cyberdemon(name, strength, hitPoints);
                    players.add(creature);
                    namePrompt = true;
                }
                else if((species.substring(0, 1).equals("e")) || (species.substring(0, 1).equals("E"))){
                    strength = (int)(Math.random() * (15) + 45);
                    hitPoints = (int)(Math.random() * (5) + 12);
                    Elf creature = new Elf(name, strength, hitPoints);
                    players.add(creature);
                    namePrompt = true;
                }
                else if((species.substring(0, 1).equals("h")) || (species.substring(0, 1).equals("H"))){
                    strength = (int)(Math.random() * (25) + 45);
                    hitPoints = (int)(Math.random() * (15) + 5);
                    Human creature = new Human(name, strength, hitPoints);
                    players.add(creature);
                    namePrompt = true;
                }
                else{
                    System.out.println("Species not recognized. Please try again\n");
                    namePrompt = false;
                }
            }
        }
	System.out.println("\nCurrent players (" + players.size() + " total):");
        System.out.println("NAME | SPECIES | STRENGTH | HIT POINTS");
        for(int i = 0; i < players.size(); i++){
            System.out.println(players.get(i).getName() + " | " + players.get(i).getSpecies() + " | " + players.get(i).getStrength() + " | " + players.get(i).getHitPoints());
        }
        System.out.println("\n");
        return players;
    }
    
    public static void fight(ArrayList<Creature> players){
        Scanner cin = new Scanner(System.in);
        
        int playerStatus[] = new int[players.size()]; //0 is normal, 1 is dead, 2 is quit
        String selection;
        boolean validTarget; //Checks if the entered target player is correct
        String playerToAttack = "";
        int damage;
        int playersAlive = players.size();
        String latestWinner = "";
        boolean gameOver = false;
        
        System.out.println("The players are ready!\nLet the battle begin!\n");
        while(!gameOver){
        for(int i = 0; i < players.size(); i++){
            if(playerStatus[i] == 0){
                System.out.println(players.get(i).getName() + ", select one of the following options:");
                System.out.println("\ta/A: Attack an opponent");
                System.out.println("\tp/P: Pass (go to the next player)");
                System.out.println("\tq/Q: Quit the game");
                System.out.print("\n" + players.get(i).getName() + ", make your selection: ");
                selection = cin.nextLine();
                
                if((selection.substring(0, 1).equals("q")) || (selection.substring(0, 1).equals("Q"))){
                    playerStatus[i] = 2;
                    System.out.println(players.get(i).getName() + " has quit the game!");
                }
                else if((selection.substring(0, 1).equals("p")) || (selection.substring(0, 1).equals("P")))
                    System.out.println(players.get(i).getName() + " has passed their turn!");
                else if((selection.substring(0, 1).equals("a")) || (selection.substring(0, 1).equals("A"))){
                    validTarget = false;
                    while(!validTarget){ 
                        System.out.print("Which player are you attacking? ");
                        playerToAttack = cin.nextLine();
                        for(int j = 0; j < players.size(); j++){
                            if(playerToAttack.equals(players.get(j).getName())){
                                validTarget = true;
                                damage = players.get(i).getDamage();
                                if(players.get(j).getName().equals(playerToAttack)){
                                    players.get(j).Damage(damage);
                                    if(players.get(j).getStrength() == 0){
                                        playerStatus[j] = 1;
                                        System.out.println(players.get(j).getName() + " has died!");
                                    }
                                }
                            }
                        }
                        if(!validTarget)
                            System.out.println("Player not found. Check your spelling and try again");
                    }
                }
            }
            if(!gameOver){
                System.out.println("\nNAME | SPECIES | STRENGTH | HIT POINTS");
                for(int k = 0; k < players.size(); k++){
                    if(playerStatus[k] < 1)
                        System.out.println(players.get(k).getName() + " | " + players.get(k).getSpecies() + " | " + players.get(k).getStrength() + " | " + players.get(k).getHitPoints());
                }
                System.out.println("\n");
            }
            playersAlive = 0;
            for(int z = 0; z < players.size(); z++){
                if(playerStatus[z] == 0){
                    playersAlive++;
                    latestWinner = players.get(z).getName();
                }
            }
            if(playersAlive <= 1)
                gameOver = true;
        }
        }
        if(gameOver)
            System.out.println(latestWinner + ", you are the only remaining player. You won!");        
    }
    
    public static void main(String[] args) {
        intro();
        ArrayList<Creature> players = addPlayers();
        fight(players);
        System.out.println("Thanks for playing!");
    }
}