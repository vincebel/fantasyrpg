/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Cyberdemon extends Demon {
    
    public Cyberdemon(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        super(newName, newStrength, newHit);
    }
    
    @Override
    public String getSpecies(){
        return "Cyberdemon";
    }  
}
