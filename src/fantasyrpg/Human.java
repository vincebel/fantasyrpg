/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Human extends Creature {
    
    public Human(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        super(newName, newStrength, newHit);
    }
    
    public String getSpecies(){
        return "Human";
    } 
}
