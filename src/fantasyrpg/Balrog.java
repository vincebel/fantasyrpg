/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Balrog extends Demon {
    
    public Balrog(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        super(newName, newStrength, newHit);
    }
    
    public String getSpecies(){
        return "Balrog";
    }
    
    @Override
    public int getDamage(){
        int damage = super.getDamage();
        int damage2 = (int)(Math.random() * super.getHitPoints());
        damage = damage + damage2;
        return damage;
    }
    
}
