/*
 * Author: Vince Belanger
 * CPS 151
 * October 17, 2016
 */
package fantasyrpg;

public class Creature {
    //Creature object instance variables
    private int hitPoints = 0; //Nonnegative
    private int strength = 0; //Nonnegative
    private String name = "";
    
    //Constructor
    public Creature(String newName, int newStrength, int newHit) throws IllegalArgumentException {
        name = newName;
        strength = newStrength;
        hitPoints = newHit;
    }

    public int getStrength(){
        return strength;
    }
    
    public void setStrength(int newStrength) throws IllegalArgumentException{
        strength = newStrength;
    }
    
    public int getHitPoints(){
        return hitPoints;
    }
    
    public void setHitPoints(int newHP) throws IllegalArgumentException{
        hitPoints = newHP;
    }
    
    public String getName(){
        return name;
    }    
    public String getSpecies(){
        return "";
    }
    
    public int getDamage(){
        int damage = (int)(Math.random() * hitPoints);
        return damage;
    }
    
    public void Damage(int damage) throws IllegalArgumentException{
        strength = strength - damage;
        if(strength < 0)
            strength = 0;
    }
    
    public boolean isDead(){
        if(strength == 0){
            return true;
        }
        return false;
    }
    
    public boolean isNamed(String aName){
        if(name == aName){
            return true;
        }
        return false;
    }
}