# README #

A turn-based text RPG that lets you play as a Balrog, Cyberdemon, Elf, or Human with different stats and chances for critical attacks.

Developed for my Algorithms & Programming II class (Fall 2016)

### ABOUT ###

* This is a turn-based text fantasy RPG that should run in any Java IDE.
* Version 1.0.0

### SETUP ###

* Import the project into any IDE that supports Java, such as NetBeans or Eclipse.
* Make sure there aren't any errors, then run the project.
* The game will start in the IDE's output window.

### GAME RULES ###

* Part 1 of the game is setting up the players and choosing your race.
* The game begins when everyone has chosen their race.
* Each round, a player gets one turn to attack whoever they chose.
* When a player's health reaches 0, they are out of the game.
* The last player remaining wins!